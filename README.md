# ics-ans-role-naming

Ansible role to install Naming Convention Tool.

## Requirements

- ansible >= 2.7
- molecule >= 2.19

## Role Variables

```yaml
naming_network: naming-network
naming_container_name: naming
naming_container_image: "{{ naming_container_image_name }}:{{ naming_container_image_tag }}"
naming_container_image_name: registry.esss.lu.se/ics-software/naming-convention-tool
naming_container_image_tag: latest
naming_container_image_pull: "True"
naming_env: {}
naming_frontend_rule: "Host:{{ ansible_fqdn }}"

# To use external database host, define 'naming_database_host'
# naming_database_host: database-host
naming_database_container_name: naming-database
naming_database_image: postgres:9.6.7
naming_database_published_ports: []
naming_database_volume: naming-database
naming_database_name: discs_names
naming_database_username: discs_names
naming_database_password: discs_names
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-naming
```

## License

BSD 2-clause
