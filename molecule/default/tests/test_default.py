import os
import testinfra.utils.ansible_runner
import pytest
import time


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.fixture(scope="module", autouse=True)
def healthcheck(host):
    for i in range(200):
        cmd = host.run("curl --insecure --fail https://ics-ans-role-naming-default/rest/healthcheck")
        if cmd.rc == 0:
            return
        time.sleep(3)
    raise RuntimeError('Timed out waiting for application to start.')


def test_application_container(host):
    with host.sudo():
        app = host.docker("naming")
        assert app.is_running


def test_application_ui(host):
    cmd = host.run("curl --insecure --fail https://ics-ans-role-naming-default/")
    assert cmd.rc == 0
    assert "<title>Naming Service</title>" in cmd.stdout


def test_database_container(host):
    with host.sudo():
        database = host.docker("naming-database")
        assert database.is_running
